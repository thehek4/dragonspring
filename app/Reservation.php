<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Reservation extends Model
{
	public $timestamps = false;
	protected $fillable = [
	'fname', 'lname', 'date', 'time', 'people', 'email', 'phone', 'info'];

	public static function availableTimes($date, $people)
	{
		$minutes = array('00', '15', '30', '45');
		$hours = array('10', '11', '12', '13', '14', '15', '16', '17', '18', '19');

		foreach ($hours as $hour) {
			foreach ($minutes as $minute) {
				$time = $hour . ':' . $minute;
				$max = $hour + 2 . ':' . $minute;
				$min = $hour - 2 . ':' . $minute;
				if ($hour - 2 < 10) {
					$min = '0' . $hour - 2 - ':' . $minute;
				}
				$reservations = self::allBetween($date, $min, $max);
				if (self::checkAvailability($reservations, $people)) {
					$available[] = $time;
				}
			}
		}

		$reservations = self::allBetween($date, '18:00', '20:00');
		if (self::checkAvailability($reservations, $people)) $available[] = '20:00';

		return $available;
	}

	public static function allBetween($date, $start, $end)
	{
		$reservations = DB::table('reservations')
			->where('date', $date)
			->whereBetween('time', [$start, $end])
			->get();
		return $reservations;
	}

	private static function checkAvailability($reservations, $people)
	{
		if (count($reservations) >= 5) {
			return false;
		}

		$allPeople = 0;
		foreach ($reservations as $reservation) {
			$allPeople += $reservation->people;
			if ($allPeople + $people > 10) {
				return false;
			}
		}

		return true;
	}
}
