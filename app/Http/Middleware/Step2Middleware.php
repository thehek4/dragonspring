<?php

namespace App\Http\Middleware;

use Closure;

class Step2Middleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $date = $request->session()->get('date');
        $people = $request->session()->get('people');

        if (!$date and !$people) {
            return redirect('/booking');
        }

        return $next($request);
    }
}
