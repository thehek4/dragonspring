<?php

namespace App\Http\Middleware;

use Closure;

class Step3Middleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $time = $request->session()->get('time');
        $date = $request->session()->get('date');
        $people = $request->session()->get('people');

        if (!$time or !$date or !$people) {
            return redirect('/booking');
        }

        return $next($request);
    }
}
