<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@getMenu');

// Route::get('/booking', function() {
// 	return redirect('/booking/step1');
// });
//
// Route::get('/booking/step1', 'BookingController@step1');
// Route::get('/booking/step2', 'BookingController@step2')->middleware(['step2']);
// Route::get('/booking/step3', 'BookingController@step3')->middleware(['step2', 'step3']);
// Route::get('/booking/success', function() {
// 	return view('booking/success');
// });
//
// Route::post('/booking/step1', 'BookingController@doStep1');
// Route::post('/booking/step2', 'BookingController@doStep2');
// Route::post('/booking/step3', 'BookingController@doStep3');

Route::get('/reservations', 'ReservationController@showReservations');
Route::get('/reservations/feed', 'ReservationController@feed');

Route::post('/reservations/{id}/destroy', ['uses' => 'ReservationController@destroy']);

Route::auth();
