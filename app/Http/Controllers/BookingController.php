<?php

namespace App\Http\Controllers;

use Validator;
use Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Reservation;

class BookingController extends Controller
{
	private $reservation;

	public static function step1()
	{
		return view('booking/step1');
	}

	public static function step2(Request $request)
	{
		$date = $request->session()->get('date');
		$people = $request->session()->get('people');

		$times = Reservation::availableTimes($date, $people);

		return view('booking/step2', ['times' => $times	]);
	}

	public static function step3()
	{
		return view('booking/step3');
	}

	public static function doStep1(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'people' => 'numeric|required|between:1,10',
			'date' => 'required'
		]);

		if ($validator->fails()) {
			return redirect('/booking/step1')
				->withErrors($validator)
				->withInput();
		}

		$date = $request->get('date');
		$people = $request->get('people');

		$request->session()->put($request->all());

		return redirect('/booking/step2');
	}

	public static function doStep2(Request $request)
	{
		$request->session()->put($request->all());
		return redirect('/booking/step3');
	}

	public static function doStep3(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'fname' => 'required|max:25',
			'lname' => 'required|max:25',
			'email' => 'required|email|',
			'phone' => 'required|numeric',
			'info' => 'max:200',
			'g-recaptcha-response' => 'required|captcha'
		]);

		if ($validator->fails()) {
			return redirect('/booking/step3')
				->withErrors($validator)
				->withInput();
		}

		$request->session()->put($request->all());
		$reservation = $request->session()->all();

		//self::sendMail($reservation);

		Reservation::create($reservation);
		$request->session()->flush();

		return view('booking/success', ['reservation' => $reservation]);
	}

	// private static function sendMail($reservation)
	// {
	// 	Mail::send('emails.reservation', ['reservation' => $reservation], function($message) use ($reservation) {
	// 		$message->from('booking@dragonspring.fi', 'Dragon Spring');
	// 		$message->to($reservation['email'])->subject('Pöytävaraus');
	// 	});
	// }

}
