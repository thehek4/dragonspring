<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Meal;

class HomeController extends Controller
{
    public static function getMenu()
    {
        $meals = Meal::allOrderByCategory();

        return view('index', ['categories' => $meals]);
    }
}
