<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

use App\Reservation;
use Auth;
use Response;

class ReservationController extends Controller
{
    public function __construct()
    {
		    $this->middleware('auth');
    }

    public static function showReservations()
    {
        $reservations = Reservation::all();

        return view('reservations/index', ['reservations' => $reservations]);
    }

    public static function feed()
    {
    	$reservations = Reservation::all();
    	$feed = array();
    	foreach ($reservations as $reservation) {
    		$feed[] = array(
    			'id' => $reservation->id,
    			'start' => date('c', strtotime($reservation->date . '' . $reservation->time)),
    			'title' => $reservation->fname . ' ' . $reservation->lname,
    			'date' => 'Pvm: ' . $reservation->date,
    			'time' => 'Klo: ' . $reservation->time,
    			'email' => 'Sähköposti: ' . $reservation->email,
    			'phone' => 'Puhelin: ' . $reservation->phone,
    			'info' => 'Lisätietoja: ' . $reservation->info,
    			'people' => 'Henkilöitä: ' . $reservation->people,
    		);
    	}
    	return json_encode($feed, JSON_PRETTY_PRINT);
    }

    public static function destroy($id)
    {
        Reservation::destroy($id);

        return redirect('/reservations');
    }
}
