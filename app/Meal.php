<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Meal extends Model
{
    public $timestamps = false;

    public static function allOrderByCategory()
    {
        $results = DB::table('meals')->select('category')->distinct()->get();

        $meals = array();
        foreach ($results as $result) {
            $category = $result->category;
            $meals[$category] = self::where('category', $category)->get();
        }

        return $meals;
    }
}
