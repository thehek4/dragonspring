# [Dragon Spring](http://dragonspring.fi/)

Dragon Spring ravintolan kotisivu, varausjärjestelmä ja hallintosivu.

## Vaatimukset ##

-----

	PHP >= 5.5.9
	OpenSSL PHP Extension
	PDO PHP Extension
	Mbstring PHP Extension
	Tokenizer PHP Extension
	SQL server(esim. MySQL)
	Composer
	Node.js

## Asennusohjeet ##

-----

* `git clone https://gitlab.com/thehek4/dragonspring.git`
* `cd dragonspring`
* `composer install`
* `npm install`
* `bower install`
* Luo tietokanta ja syötä sen tiedot *.env* -tiedostoon. Mallina *.env.example*
* `php artisan key:generate`
* `gulp`
* `php artisan migrate --seed` Luo tietokantataulut ja dataa
* `php artisan serve` sovellus osoitteessa http://localhost:8000/


## Toiminnot ##

-----

* Kotisivu `/`
* Pöytävaraus `/booking` (booking branch)
* Hallintosivu `/reservations` käyttäjätunnus : `testaaja` salasana : `salasana` (booking branch)