<?php

use Illuminate\Database\Seeder;

use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->delete();

      $password = bcrypt('salasana');
      User::create(array('username' => 'testaaja', 'password' => $password));
    }
}
