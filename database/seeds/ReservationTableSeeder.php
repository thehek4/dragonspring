<?php

use Illuminate\Database\Seeder;

use App\Reservation;

class ReservationTableSeeder extends Seeder
{

    public function run()
    {
      DB::table('reservations')->delete();

      $timestamp = date('Y-m-d H:i:s');
      $date = substr($timestamp, 0, 10);
      $time = substr($timestamp, 11);

      Reservation::create(array(
        'fname' => 'Seppo', 'lname' => 'Taalasmaa', 'date' => $date, 'time' => $time, 'people' => 5, 'email' => 'seppo@taalasmaa.fi', 'phone' => '123'));
      Reservation::create(array(
        'fname' => 'Seppo', 'lname' => 'Taalasmaa', 'date' => $date, 'time' => $time, 'people' => 5, 'email' => 'seppo@taalasmaa.fi', 'phone' => '123'));
    }
}
