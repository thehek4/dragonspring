<?php

use Illuminate\Database\Seeder;
use App\Meal;

class MealsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('meals')->delete();

        $id = 1;
        for ($i=1; $i < 16; $i++) {
          for ($j=0; $j < 5; $j++) {
            Meal::create(array(
              'number' => $id,
              'name' => 'LOREM IPSUM DOLOR ASDFDSFA',
              'price' => 10.50,
              'category' => 'Category' . $i,
            ));
            $id++;
          }
        }

        for ($j=1; $j < 6; $j++) {
          Meal::create(array(
            'number' => 'S' . $j,
            'name' => 'LOREM IPSUM ASDFASFA',
            'price' => 10.50,
            'category' => 'Category X',
            'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s."
          ));
        }

    }
}
