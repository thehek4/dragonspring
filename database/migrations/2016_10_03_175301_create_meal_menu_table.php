<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMealMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meal_menu', function(Blueprint $table) {
          $table->integer('menu_id')->unsigned();
          $table->integer('meal_id')->unsigned();

          $table->foreign('menu_id')->references('id')->on('menus');
          $table->foreign('meal_id')->references('id')->on('meals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('meal_menu');
    }
}
