@extends('layouts/main')

@section('content')

<!-- Intro Header -->
<header class="intro">
  <div class="intro-body">
    <div class="container">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <h1 class="brand-heading">Dragon Spring</p>
          <h1 class="brand-intro">龍泉餐館</h1>

          <p class="intro-text"><b>Kiinalainen ravintola | Urho Kekkosen Katu 4 | Helsinki</b></p>
          <a href="#about" class="btn btn-circle page-scroll">
            <i class="fa fa-angle-double-down animated"></i>
          </a>
        </div>
      </div>
    </div>
  </div>
</header>

<!-- About Section -->
<section id="about" class="container content-section text-center">
  <div class="row">
    <div class="col-lg-8 col-lg-offset-2">

      <h1>Tietoa meistä</h1>
      <p>Ravintolamme palvelee teitä Helsingin keskustassa yli 16 vuoden kokemuksella. Ruokalistallamme on yli 120 eri ruoka-annosta. Meiltä löytyy loistavia tarjouksia (klo 11:00 - 20:00), ja tarjoukset vaihtuvat päivittäin.  </p>
      <p>Sijaitsemme Helsingin ydinkeskustassa kauppakeskus Kampin vieressä, hyvien kulkuyhteyksien varrella. </p>
      <p>Tervetuloa ravintolaamme.</p>
 </div>
  </div>

  <div class="row thumbnails">
    <img src="/img/83.jpg" alt="food" width="350px">
    <img src="/img/108.jpg" alt="food" width="350px">
    <img src="/img/A-a.jpg" alt="food" width="350px">
  </div>
</section>

<!-- Menu Section -->
<section id="menu" class="content-section">
  <div class="menu-section">
    <div class="container">
      <div class="col-lg-10 col-lg-offset-1">
        <!-- <h1 class="text-center">Menu</h1> -->
        <div class="list-group list-group-root well" aria-multiselectable="true">

          <div class="list-group-item menu-heading">
            <h2 class="list-group-item-heading text-center category">Menu</h2>
          </div>          

          @php($i = 1)
          @foreach ($categories as $category => $meals)
          <div>
            <a class="list-group-item" data-toggle="collapse" href="#Collapse{{ $i }}" aria-expanded="true">
              <h4 class="list-group-item-heading text-center category">
                {{ $category }}
                <i class="glyphicon glyphicon-triangle-bottom pull-right"></i>
              </h4>
            </a>

            <div class="list-group collapse" id="Collapse{{ $i }}">

               @foreach ($meals as $meal)
               <div class="list-group-item menu-group-item">

                 @if($meal->description)
                 <p class="list-group-item-heading meal-name">{{ $meal->number }}. <strong>{{ $meal->name }}</strong></p>
                 <div class="row">
                   <p class="meal-description list-group-item-text col-sm-10">{{ $meal->description }}</p>
                   <p class="price col-sm-2 list-group-item-text">{{ $meal->price }} €</p>
                 </div>
                 @else
                 <div class="list-group-item-heading row list-group-item-without-description">
                   <p class="col-sm-10 meal-name">{{ $meal->number }}. <strong>{{ $meal->name }}</strong></p>
                   <p class="price col-sm-2">{{ $meal->price }} €</p>
                 </div>
                 @endif

               </div>
               @endforeach

            </div>
          </div>
          @php($i++)
          @endforeach

        </div>
      </div>
    </div>
  </div>
</section>

<!-- Map Section -->
<section id="contact" class="content-section clearfix">
  <h1 class="text-center">Yhteystiedot</h1>
  <div>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1984.7209181709006!2d24.93121325199353!3d60.168798881871915!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46920a34acf49c49%3A0xe418c9139c4a33f0!2sDragon+Spring!5e0!3m2!1sen!2scm!4v1474051968120" width="100%" height="400px" frameborder="0"></iframe>
  </div>
  <div class="container">
    <div class="col-md-8 col-md-offset-2 info-list">
      <ul class="list-unstyled" id="info">
        <li><h4>Urho Kekkosen katu 4</h4></li>
        <li><h4>Helsinki, 00101</h4></li>
        <li><h4>Puh - (09)6942738</h4></li>
      </ul>
      <ul class="list-unstyled" id="opening">
        <li><h4>ma - to : 11:00 - 22:00</h4></li>
        <li><h4>pe : 11:00 - 23:00</h4></li>
        <li><h4>la : 12:00 - 23:00</h4></li>
        <li><h4>su : 12:00 - 22:00</h4></li>
      </ul>
    </div>
  </div>
</section>

@stop
