@extends('layouts/booking')

@section('content')

<h3>Tarkista saatavuus</h3>
<hr>

<form class="form-horizontal" action="{{ url('/booking/step1') }}" method="post">
  {{ csrf_field() }}

  <div class="form-group">
    <div class="panel panel-default">
      <div class="panel-heading">
        <span class="glyphicon glyphicon-calendar"></span>
        <strong class="panel-title"> Valitse päivämäärä </strong>
      </div>
      <div class="panel-body">
        <div id="datetimepicker">
          <input type="text" name="date" hidden="hidden">
        </div>
      </div>
    </div>
  </div>

  <div class="col-sm-6">
    <div class="form-group">
      <label for="people" class="control-label col-sm-4">Henkilöitä</label>
      <div class="col-sm-8">
        <input class="form-control" type="number" max="12" min="1" name="people" value="{{ old('people') }}">
        @if ($errors->has('people'))
        <span class="help-block">
          <strong class="text-danger">{{ $errors->first('people') }}</strong>
        </span>
        @endif
      </div>
    </div>
  </div>

  <button class="btn btn-primary nextBtn btn-lg pull-right" type="submit">Seuraava</button>
</form>

@stop
