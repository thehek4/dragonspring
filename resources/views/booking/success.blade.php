@extends('layouts/booking')

@section('content')

<div class="alert alert-success" role="alert">
	<strong>Pöytävaraus onnistui!</strong>
	<br>
	<p>Lähetämme varaustietosi antamasi sähköpostiosoitteeseen.</p>
</div>

@stop
