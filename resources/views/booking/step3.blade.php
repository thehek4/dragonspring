@extends('layouts/booking')

@section('content')

<h3>Yhteystiedot</h3>

<hr>

<form class="form-horizontal" action="{{ url('/booking/step3') }}" method="post">
  {{ csrf_field() }}

  <div class="form-group">
    <label for="fname" class="col-sm-2 control-label">Etunimi</label>
    <div class="col-sm-10">
      <input class="form-control" type="text" name="fname" value="{{ old('fname') }}">
      @if ($errors->has('fname'))
      <span class="help-block">
        <strong class="text-danger">{{ $errors->first('fname') }}</strong>
      </span>
      @endif
    </div>
  </div>

  <div class="form-group">
    <label for="lname" class="control-label col-sm-2">Sukunimi</label>
    <div class="col-sm-10">
      <input class="form-control" type="text" name="lname" value="{{ old('lname') }}">
      @if ($errors->has('lname'))
      <span class="help-block">
        <strong class="text-danger">{{ $errors->first('lname') }}</strong>
      </span>
      @endif
    </div>
  </div>

  <div class="form-group">
    <label for="email" class="control-label col-sm-2">Sähköposti</label>
    <div class="col-sm-10">
      <input class="form-control" type="email" name="email" value="{{ old('email') }}">
      @if ($errors->has('email'))
      <span class="help-block">
        <strong class="text-danger">{{ $errors->first('email') }}</strong>
      </span>
      @endif
    </div>
  </div>

  <div class="form-group">
    <label for="phone" class="control-label col-sm-2">Puhelinnumero</label>
    <div class="col-sm-10">
      <input class="form-control" type="text" name="phone" value="{{ old('phone') }}">
      @if ($errors->has('phone'))
      <span class="help-block">
        <strong class="text-danger">{{ $errors->first('phone') }}</strong>
      </span>
      @endif
    </div>
  </div>

  <div class="form-group">
    <label for="info" class="control-label col-sm-2">Lisätietoja</label>
    <div class="col-sm-10">
      <textarea class="form-control" type="text" name="info" value="{{ old('info') }}" rows="6"></textarea>
      @if ($errors->has('info'))
      <span class="help-block">
        <strong class="text-danger">{{ $errors->first('info') }}</strong>
      @endif
      </span>
    </div>
  </div>

  <div class="form-group">
      <div class="col-sm-offset-4">
        {!! app('captcha')->display(); !!}
        @if ($errors->has('g-recaptcha-response'))
        <span class="help-block">
          <strong class="text-danger">{{ $errors->first('g-recaptcha-response') }}</strong>
        @endif
      </div>
  </div>

  <a href="{{ url('/booking/step2') }}" class="btn btn-lg btn-primary pull-left">Takaisin</a>
  <button class="btn btn-success btn-lg pull-right" type="submit" class="btn btn-default">Varaa</button>
</form>

@stop
