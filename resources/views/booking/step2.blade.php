@extends('layouts/booking')

@section('content')

<h3>Valitse ajankohta</h3>
<hr>

<form action="{{ url('/booking/step2') }}" method="post">
  {{ csrf_field() }}

  <div class="panel panel-default">
    <div class="panel-heading">
      <span class="glyphicon glyphicon-time"></span>
      <strong class="panel-title"> Vapaat ajat: </strong>
    </div>
    <div class="panel-body">
      <div class="buttons">
      @foreach ($times as $time)
        <input type="submit" class="btn btn-primary btn-lg timebutton" name="time" value="{{ $time }}">
      @endforeach
      </div>
    </div>
  </div>

  <a href="{{ url('/booking/step1') }}" class="btn btn-lg btn-primary pull-left">Takaisin</a>
</form>

@stop
