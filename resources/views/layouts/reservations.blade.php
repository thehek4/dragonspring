<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Dragon Spring</title>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">

  </head>

  <body>

    <nav class="navbar navbar-inverse" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{ url('/')}}">Dragon Spring</a>
        </div>

        <div class="collapse navbar-collapse" id="navigation">
          @if (Auth::check())
          <ul class="nav navbar-nav">
          	<li><a href="">Varaukset</a></li>
          </ul>
          <p class="navbar-text navbar-right">
          	<span class="glyphicon glyphicon-user"></span>
          	<a href="{{ url('/logout') }}" class="navbar-link">Kirjaudu ulos</a>
          </p>
          @endif
        </div>
      </div>
    </nav>

    <div class="container">
      @yield('content')
    </div>

    <!-- <script type="text/javascript" src="{{ url('js/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/moment-with-locales.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/fullcalendar.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/fi.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/reservations.js') }}"></script> -->

    <script src="{{ asset('js/admin.js') }}" charset="utf-8"></script>

  </body>
</html>
