<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Dragon Spring</title>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- <link href="{{ url('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('css/booking.css') }}" rel="stylesheet">
    <link href="{{ url('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet"> -->

    <link rel="stylesheet" href="{{ asset('css/booking.css') }}">

    <script src='https://www.google.com/recaptcha/api.js'></script>
  </head>

  <body>

    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="btn btn-default navbar-btn" href="{{ url('/') }}">
            <span class="glyphicon glyphicon-chevron-left"></span>
            Etusivulle
          </a>
        </div>
      </div>
    </nav>

    <div class="container">

      <div class="page-header">
        <h1 class="text-center">Pöytävaraus</h1>
      </div>

      <div class="col-lg-8 col-lg-offset-2">

        @yield('content')

      </div>

    </div><!-- Container -->

    <script src="{{ asset('/js/booking.js') }}" charset="utf-8"></script>

  </body>
</html>
