@extends('layouts/reservations')

@section('content')
<div class="mini-container">
  <div class="panel panel-default login">
    <div class="panel-heading"><span class="glyphicon glyphicon-lock"></span></div>
    <div class="panel-body">
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2 vcenter">

          <form action="{{ url('/login') }}" method="post">
            {{ csrf_field() }}
            <!-- Tekstikenttä käyttäjätunnukselle -->
            <div class="form-group">
              <label>Käyttäjätunnus</label>
              <input type="text" class="form-control" name="username" value="{{ old('username') }}">

              @if ($errors->has('username'))
              <span class="help-block">
                <strong class="text-danger">{{ $errors->first('username') }}</strong>
              </span>
              @endif

            </div>

            <!-- Salasanakenttä salasanalle -->
            <div class="form-group">
              <label>Salasana</label>
              <input type="password" class="form-control" name="password" value="">

              @if ($errors->has('password'))
              <span class="help-block">
                <strong class="text-danger">{{ $errors->first('password') }}</strong>
              </span>
              @endif

            </div>

            <button type="submit" class="btn btn-primary">Kirjaudu sisään</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
