@extends('layouts/reservations')

@section('content')

<div class="page-header">
	<h1>Varaukset</h1>
</div>

<div id="calendar"></div>

<div id="calendarModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
				<h4 id="modalTitle" class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<p id="eventDate"></p>
				<p id="eventTime"></p>
				<p id="eventPeople"></p>
				<p id="eventEmail"></p>
				<p id="eventPhone"></p>
				<p id="eventInfo"></p>
			</div>
			<div class="modal-footer">
				<div class="row">
					<div class="pull-right">
						<button type="button" class="btn btn-default" id="closeButton" data-dismiss="modal">Sulje</button>
					</div>
					<form id="destroyEvent" method="post">
						{{ csrf_field() }}
						<button type="submit" class="btn btn-danger">Poista</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@stop
