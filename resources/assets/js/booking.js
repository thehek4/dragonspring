$(function() {
  $('#datetimepicker1').datetimepicker({
    locale: 'fi',
    format: 'DD.MM.YYYY',
    minDate: moment(),
    maxDate: moment().add(1, 'month'),
    useCurrent: false,
    allowInputToggle: true,
  });
});

$(function () {
  $('#datetimepicker').datetimepicker({
    locale: 'fi',
    format: 'YYYY-MM-DD',
    minDate: moment(),
    maxDate: moment().add(1, 'month'),
    inline: true,
  });
});
