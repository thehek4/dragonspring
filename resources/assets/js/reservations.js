$(document).ready(function() {

    // page is now ready, initialize the calendar...

    $('#calendar').fullCalendar({
        // put your options and callbacks here
        header: {
         left:   'today prev,next',
         center: 'title',
         right:  'month,agendaWeek,agendaDay',
       },
       lang: 'fi',
       allDaySlot: false,
       defaultView: 'agendaWeek',
       minTime: '10:00',
       maxTime: '21:00',
       height: 'auto',
       slotEventOverlap: false,
       slotLabelFormat: 'H:mm',
       events: '/reservations/feed',
       eventClick:  function(event, jsEvent, view) {
           $('#modalTitle').html(event.title);
           $('#eventDate').html(event.date);
           $('#eventTime').html(event.time);
           $('#eventPeople').html(event.people);
           $('#eventEmail').html(event.email);
           $('#eventPhone').html(event.phone);
           $('#eventInfo').html(event.info);
           $('#eventUrl').attr('href',event.url);
           $('#calendarModal').modal();

           var uri = "/reservations/" + event.id + "/destroy";
           $('#destroyEvent').attr('action', uri);
       },
     });
  });
