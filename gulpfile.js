var elixir = require('laravel-elixir');
var gulp = require('gulp');
var bower = require('gulp-bower');

gulp.task('bower', function() {
    return bower();
});

var vendors = '../../vendor/';

var paths = {
    'jquery': vendors + '/jquery/dist/jquery.js',
    'bootstrap': vendors + '/bootstrap/dist',
    'fontawesome': vendors + '/font-awesome/css/font-awesome.css',
    'fullcalendar': vendors + '/fullcalendar/dist',
    'locale': vendors + '/fullcalendar/dist/locale',
    'datetimepicker': vendors + '/eonasdan-bootstrap-datetimepicker/build',
    'jqueryEasing': vendors + '/jquery.easing/js/jquery.easing.js',
    'moment': vendors + '/moment'
};

elixir.config.sourcemaps = false;

elixir(function(mix) {

  // Run bower install
  mix.task('bower');

  // Copy fonts straight to public
  mix.copy('resources/vendor/bootstrap/dist/fonts/**', 'public/fonts');
  mix.copy('resources/vendor/font-awesome/fonts/**', 'public/fonts');

  //Merge Site css
  mix.styles([
    paths.bootstrap + '/css/bootstrap.css',
    paths.fontawesome,
    'index.css',
  ], 'public/css/site.css');

  //Merge Booking css
  mix.styles([
    paths.bootstrap + '/css/bootstrap.css',
    paths.datetimepicker + '/css/bootstrap-datetimepicker.css',
    'booking.css',
  ], 'public/css/booking.css');

  //Merge Admin css
  mix.styles([
    paths.bootstrap + '/css/bootstrap.css',
    paths.fullcalendar + '/fullcalendar.css',
    'reservations.css',
  ], 'public/css/admin.css');

  //Merge Site js
  mix.scripts([
    paths.jquery,
    paths.jqueryEasing,
    paths.bootstrap + '/js/bootstrap.js',
    'index.js',
  ], 'public/js/site.js');

  //Merge Booking js
  mix.scripts([
    paths.jquery,
    paths.bootstrap + '/js/bootstrap.js',
    paths.moment + '/min/moment-with-locales.js',
    paths.datetimepicker + '/js/bootstrap-datetimepicker.min.js',
    'booking.js',
  ], 'public/js/booking.js');

  //Merge Admin js
  mix.scripts([
    paths.jquery,
    paths.bootstrap + '/js/bootstrap.js',
    paths.moment + '/min/moment-with-locales.js',
    paths.fullcalendar + '/fullcalendar.js',
    paths.locale + '/fi.js',
    'reservations.js',
  ], 'public/js/admin.js');

});
